import random



Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
Prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]
random_element = random.choice
noun1 = random.choice(Nouns)
noun2 = random.choice(Nouns)
noun3 = random.choice(Nouns)
verb1 = random.choice(Verbs)
verb2 = random.choice(Verbs)
verb3 = random.choice(Verbs)
adj1 = random.choice(Adjectives)
adj2 = random.choice(Adjectives)
adj3 = random.choice(Adjectives)
prep1 = random.choice(Prepositions)
prep2 = random.choice(Prepositions)
adverb1 = random.choice(Adverbs)
if adj1[0] == "f" or adj1[0] == "b" or adj1[0] == "g":
    A = "A"
else:
    A = "An"
print("Challenge: Wax Poetic")
print("")
print(f"{A} {adj1} {noun1}")
print(f"{A} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}")
print(f"{adverb1}, the {noun1} {verb2}")
print(f"the {noun2} {verb3} {prep2} a {adj3} {noun3}")

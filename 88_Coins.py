import random
print("Challenge: Simulate a Coin Toss Experiment")
variable = "0"

def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


solution = 0
zasieg = 10000
for trial in range(zasieg):
    a1 = coin_flip()
    solution += 1
    while a1 ==  coin_flip():
        solution += 1
    solution += 1

wynik = solution/zasieg
print(f"Twój wynik to: {wynik}!")
print("THE END")
print("THANK YOU")
koniec = input("Wciśnij ENTER aby wyjść")

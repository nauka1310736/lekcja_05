def enrollment_stats(x):
    student_enrollment_values = []
    tuition_fees = []
    for y in x:
        student_enrollment_values.insert(0, y[1])
        tuition_fees.insert(0, y[2])
    return student_enrollment_values, tuition_fees

def mean(x):
    suma1 = 0
    suma2 = 0
    for y in x:
        suma1 += y[1]
    mean_students = suma1/len(x)
    for y in x:
        suma2 += y[2]
    mean_tuition = suma2/len(x)
    return mean_students, mean_tuition

def median(x):
    student_enrollment_values = []
    tuition_fees = []
    for y in x:
        student_enrollment_values.insert(0, y[1])
        tuition_fees.insert(0, y[2])
    student_enrollment_values.sort()
    tuition_fees.sort()
    
    if len(tuition_fees)%2 == 0:
        a = student_enrollment_values[int((len(student_enrollment_values)/2)-1)]
        b = student_enrollment_values[int(len(student_enrollment_values)/2)]
        mediana_student_enrollment_values = (a + b)/2
        c = tuition_fees[int((len(tuition_fees)/2)-1)]
        d = tuition_fees[int(len(tuition_fees)/2)]
        mediana_tuition_fees = (c + d)/2
    else:
        mediana_student_enrollment_values = student_enrollment_values[int(((len(student_enrollment_values)-1)/2))]
        mediana_tuition_fees = tuition_fees[int(((len(tuition_fees)-1)/2))]
    return mediana_student_enrollment_values, mediana_tuition_fees
    
        
print("Challenge: List of lists")
universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]
enrollment_stats = enrollment_stats(universities)
liczba_studentow = sum(enrollment_stats[0])
suma_czesne = sum(enrollment_stats[1])
mean = mean(universities)
mean_students = mean[0]
mean_tuition = mean[1]
median_student = median(universities)

print("******************************")
print(f"Total students: {liczba_studentow}")
print(f"Total tuition: $ {suma_czesne}")
print("")
print(f"Student mean: {mean_students:.2f}")
print(f"Student median:  {median_student[0]}")
print("")
print(f"Tuition mean: ${mean_tuition:.2f}")
print(f"Tuition median: $ {median_student[1]}")
print("******************************")



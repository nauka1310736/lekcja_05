import random
def losowanie(probability):
    if random.random() < probability:
        return 1
    else:
        return 0
print("Challenge: Simulate an Election")
wynik_losowania = 0
wynik_wyborow = 0
zakres = 10000
for trial in range(zakres):
    if losowanie(.87) == 1:
        wynik_losowania += 1
    if losowanie(.65) == 1:
        wynik_losowania += 1
    if losowanie(.17) == 1:
        wynik_losowania += 1
    if wynik_losowania > 1:
        wynik_wyborow += 1
        wynik_losowania = 0
    wynik_losowania = 0
wynik_koncowy = wynik_wyborow/zakres
print(f"Prawdopodobieństwo wygrania kandydata A po przeprowadzeniu \nsymulacji na 10000 próbach wynosi: {wynik_koncowy}")

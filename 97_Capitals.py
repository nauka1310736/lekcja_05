import random
print("9.7 Challenge: Capital City Loop")
capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}
losowy_stan, losowa_stolica = random.choice(list(capitals_dict.items()))
losowa_stolica = losowa_stolica.upper()
i = 1
while i != losowa_stolica:
    i = input(f"Enter the capital {losowy_stan} or exit:\n")
    i = i.upper()
    if i == "EXIT":
        print("Goodbye")
        break
    if i == losowa_stolica:
        print("Correct!!!")
    else:
        print("Bad!!!")

